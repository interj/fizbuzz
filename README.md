# This is just stupid enough to share:

```zsh
x(){(($i%$1==0))&&i=$2;};F=Fizz;B=Buzz;for i ({0..99}){x 15 $F$B||x 3 $F||x 5 $B;echo $i}
```

 Runs with zsh 5.4.2
 
 0 is considered divisible by 15 (and rightly so!)
 
 _I do not condone or encourage writing such code other than for kicks and giggles._